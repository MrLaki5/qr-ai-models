# QR-AI-models
This repo contains models used in [QR-AI-generator repo](https://github.com/MrLaki5/QR-AI-generator)

## Model origins
* stable_diffusion_v1_5: https://huggingface.co/runwayml/stable-diffusion-v1-5
* controlnet_v1p_sd15: https://huggingface.co/DionTimmer/controlnet_qrcode-control_v1p_sd15
